const faverage = (numbers) => {

    if (numbers == null || numbers === undefined || numbers.length == 0 ) {
      return 0;
    }
    number = numbers.reduce((a, b)=> a + b)
    
    return number/numbers.length;

};

module.exports = faverage;
