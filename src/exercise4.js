const wordCount = sequence => {
  if (sequence=== undefined)
    return null;
    
  dic = {}
  seq = sequence.split(" ").filter(x => x.length >= 1).forEach(
    element => {
      if (dic[element.toLowerCase()] === undefined) {
        dic[element.toLowerCase()] = 1
      } else {
        dic[element.toLowerCase()] = dic[element.toLowerCase()] +1
      }
    })
  
  return Object.keys(dic).length > 0 ?  dic : null;
};

module.exports = {
  wordCount,
};
