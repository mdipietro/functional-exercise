/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => {
  const aux = (number) => {
    return (fn(number+h) - fn(number-h)) / (2*h)
  }

  return aux;
}

const squear = x => {
  return Math.pow(x, 2);
}

module.exports = {
  fderive,
  squear,
};
